# @pushrocks/smartnginx
control nginx from node, TypeScript ready

## Availabililty and Links
* [npmjs.org (npm package)](https://www.npmjs.com/package/@pushrocks/smartnginx)
* [gitlab.com (source)](https://gitlab.com/pushrocks/smartnginx)
* [github.com (source mirror)](https://github.com/pushrocks/smartnginx)
* [docs (typedoc)](https://pushrocks.gitlab.io/smartnginx/)

## Status for master
[![build status](https://gitlab.com/pushrocks/smartnginx/badges/master/build.svg)](https://gitlab.com/pushrocks/smartnginx/commits/master)
[![coverage report](https://gitlab.com/pushrocks/smartnginx/badges/master/coverage.svg)](https://gitlab.com/pushrocks/smartnginx/commits/master)
[![npm downloads per month](https://img.shields.io/npm/dm/@pushrocks/smartnginx.svg)](https://www.npmjs.com/package/@pushrocks/smartnginx)
[![Known Vulnerabilities](https://snyk.io/test/npm/@pushrocks/smartnginx/badge.svg)](https://snyk.io/test/npm/@pushrocks/smartnginx)
[![TypeScript](https://img.shields.io/badge/TypeScript->=%203.x-blue.svg)](https://nodejs.org/dist/latest-v10.x/docs/api/)
[![node](https://img.shields.io/badge/node->=%2010.x.x-blue.svg)](https://nodejs.org/dist/latest-v10.x/docs/api/)
[![JavaScript Style Guide](https://img.shields.io/badge/code%20style-prettier-ff69b4.svg)](https://prettier.io/)

## Usage

We recommend the use of TypeScript! :)

```typescript
import * as smartnginx from 'smartnginx';
const smartnginxInstance = new smartnginx.SmartNginx();
myNginxHost = new smartnginx.NginxHost({
  hostName: 'some.example.com',
  type: 'reverseProxy',
  destination: '192.192.192.192' // some destination IP
});
myNginxConfig.addZone(myNginxZone); // adds the zone to the config
myNginxConfig.deploy(); // deploys the referenced NginxConfig and gracefully reloads it
```

For further information read the linked docs at the top of this readme.

> MIT licensed | **&copy;** [Lossless GmbH](https://lossless.gmbh)
| By using this npm module you agree to our [privacy policy](https://lossless.gmbH/privacy)

[![repo-footer](https://lossless.gitlab.io/publicrelations/repofooter.svg)](https://maintainedby.lossless.com)
