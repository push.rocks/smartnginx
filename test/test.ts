import { tap, expect } from '@pushrocks/tapbundle';
import path = require('path');

import { Qenv } from '@pushrocks/qenv';
const testQenv = new Qenv('./', './.nogit/');

import * as smartnginx from '../ts/index';

let testSmartNginx: smartnginx.SmartNginx;
let testNginxZone01: smartnginx.NginxHost;
let testNginxZone02: smartnginx.NginxHost;

tap.test('should create a valid instance of SmartNginx', async () => {
  testSmartNginx = new smartnginx.SmartNginx({ defaultProxyUrl: 'https://git.zone' });
  expect(testSmartNginx).to.be.instanceof(smartnginx.SmartNginx);
});

tap.test(`should produce an instance of NginxConfig`, async () => {
  testNginxZone01 = new smartnginx.NginxHost(testSmartNginx, {
    hostName: 'test100.bleu.de',
    destination: '192.192.192.191',
    destinationPort: 3000,
    privateKey: 'some private',
    publicKey: 'some public'
  });
  testNginxZone02 = new smartnginx.NginxHost(testSmartNginx, {
    hostName: 'test102.bleu.de',
    destination: '192.192.192.192',
    destinationPort: 3050,
    privateKey: 'some private',
    publicKey: 'some public'
  });
  expect(testNginxZone01).to.be.instanceof(smartnginx.NginxHost);
});

tap.test('.addHostCandidate() should add a zone to NginxConfig Object', async () => {
  testSmartNginx.addHostCandidate(testNginxZone01);
  testSmartNginx.addHostCandidate(testNginxZone02);
});

tap.test('.deploy() should deploy a config from an instance', async () => {
  await testSmartNginx.deploy();
});

tap.test('should not redeploy', async () => {
  testSmartNginx.addHostCandidate(testNginxZone01);
  testSmartNginx.addHostCandidate(testNginxZone02);
  await testSmartNginx.deploy();
});

tap.test('.stop() should end the process', async () => {
  testSmartNginx.nginxProcess.stop();
});

tap.start();
