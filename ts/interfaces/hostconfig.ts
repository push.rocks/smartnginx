export interface IHostConfig {
  hostName: string;
  destination: string;
  destinationPort: number;
  privateKey: string;
  publicKey: string;
}
