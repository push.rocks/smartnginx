import * as plugins from './smartnginx.plugins';

// directories
export const packageBase = plugins.path.join(__dirname, '../');
export const nginxConfigDirPath = plugins.path.join(packageBase, 'nginxconfig');
export const nginxHostDirPath = plugins.path.join(nginxConfigDirPath, 'hosts');

// files
export const nginxConfFile = plugins.path.join(nginxConfigDirPath, 'nginx.conf');
